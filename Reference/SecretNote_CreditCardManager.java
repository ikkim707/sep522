class CardInfo
{
  
  String cardName;
  String cardNumber;
  String validYear;
  String validMonth;
  String cvc;
}


class CreditCardManager 
{
    private int ConvertError(int DBAErr) 
    {
          // DBAdapter의 error code를 IRS의 에러코드표를 참조하여, CreditCard Manager의 error code로 변환한다.
        
         // return error;
    }
  
  	public int  saveCreditCardInfo(String CardName, String CardNumber, String ValidYear, String ValidMonth, String Cvc, boolean addFlag )
  	{
  		//  신용 카드 정보 추가 화면에서 입력받은 사용자 입력. 
  		// 각 input의 validity는 CreditCardUI에서 check되어 입력된다.    
      
  
  		ret = saveCreditCardInfoToDb(CardName, CardNumber, ValidYear, ValidMonth, Cvc, addFlag);
  
  		if (ret == 0) {
  			// SUCCESS
  			return 0;	
  		} else {
  		  // failure
        return ConvertError(ret);
  		}
  
  	}
  	
  	public int viewCreditCardInfo(String CardName,/* out */ CardInfo cardInfo )
  	{
       // CardName은 신용 카드 목록 화면에서 선택한 이름이므로, valid하다.
       
       // DB Adapter에서 select한 결과를 cardInfo에 저장한다.
       
       ret = GetCreditCardInfo(CardName,/* out */ cardInfo);

  		if (ret == 0) {
  			// SUCCESS
  			return 0;	
  		} else {
  		  // failure
        return ConvertError(ret);
  		}       
      
 	  }


  	public int deleteCreditCardInfo( int numOfCards, String[] CardName)
  	{
       // CardName은 신용 카드 목록 화면에서 선택한 이름이므로, valid하다.
       
       ret = deleteCreditCardInfoFromDB(numOfCards, CardName);

  		if (ret == 0) {
  			// SUCCESS
  			return 0;	
  		} else {
  		  // failure
        return ConvertError(ret);
  		}       
       
  	  
 	  }
  	
  	public int searchCreditCardInfo( String searchString, /* out */ String[] CardName )
  	{

       ret = searchCreditCardInfoFromDB(searchString, /* out */CardName);
       
       
  		if (ret == 0) {
  			// SUCCESS
  			return 0;	
  		} else {
  		  // failure
        return ConvertError(ret);
  		}       
       
       
 	  }
  	
  	public int getCreditCardList( /* out */ String[] CardName )
  	{

       ret = getCreditCardListFromDB(/* out */ CardName);
       
       
  		if (ret == 0) {
  			// SUCCESS
  			return 0;	
  		} else {
  		  // failure
        return ConvertError(ret);
  		}       
       
       
 	  }
  	
}